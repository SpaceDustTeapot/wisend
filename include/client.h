#ifndef CLIENT_H
#define CLIENT_H
#include <SFML/Network.hpp>


class client
{
    public:
        client();
        virtual ~client();
        //listen for IP Broadcasts.
        void listen();
        //listen for packets sent to 777
        void recieve();
        //Recieve and store data;
        void recData();
        void cliLoop();
        //send data packets to a computer with 777
        void Send();
    protected:
    private:
    uint16_t state;
    //udp 0 for listening on 1911
    //udp 1 for listening/sending on 777
    sf::UdpSocket udp[1];
    //support the logging of 255 ip addresses/addresses
    sf::IpAddress Ipaddresses[255];
    std::string compnames[255];
};

#endif // CLIENT_H
