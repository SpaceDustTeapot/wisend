#ifndef SERVER_H
#define SERVER_H
#include<SFML/Network.hpp>
#include<string.h>
#include<iostream>
#include <unistd.h>
#include <fstream>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <string.h>

//server annouces
//Server checks
//server waits for a min
//server starts again

//Fork into three?
class server
{
    public:
        server();
        virtual ~server();
        void setSerType(uint8_t sertype);
        void setComputername(std::string setComName);
        bool createConfig();
        bool readConfig();
        bool Loadsetts(std::string token);
        void Broadcast();
        void Broad_LOOP();
        void Listen();
        void Startup();
    protected:
    private:
    //Server stuff
    sf::Uint16 serType;
    uint16_t readableSertype;
    uint16_t State;
    std::string Computername;
    sf::Packet pack;
    sf::UdpSocket udp;

    //save Directory
   std::string saveDirectory;

   //forking stuff
   pid_t pid;
};

#endif // SERVER_H
