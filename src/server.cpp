#include "../include/server.h"

//1911 = Braodcast port
//777 = data port

server::server()
{
    //ctor
    State = 1;
}

void server::setSerType(uint8_t sertype)
{
    serType = sertype;
}

void server::Startup()
{
 //fork off into broadcast thread and data thread
  pid = fork();

  if(pid == 0)
  {
   std::cout<<"[Success]Child Process sucessfully"<<std::endl;
   //loads/creates config and goes to the broadcast infinityloop
   createConfig();

  }
  else if(pid > 0)
  {
   //parent
   //start instance of client class
   //listen  on port 777
   //wait for a send, File and Size packet
   //both client and server pipe to the boradcast process they're busy
   //wait till Server sends a GOT IT Code
   //Server Chunks up the file
   //server sends it piece by peice while client sends a got it
   //at the end server sends a end file
   //client sends a got it and both close connection


  }


}

void server::setComputername(std::string setComName)
{
 Computername = setComName;

}

bool server::createConfig()
{

std::string configloc = "";
#if _WIN32
configloc = getenv("APPDATA");
configloc = configloc + "\\.wisend\\config.conf";
#else
configloc = getenv("HOME");
std::string saveLoc = configloc + "/Downloads/wisend/recieved";
std::string savelll = configloc + "/Downloads/wisend";
std::string config = configloc + "/.wisend";
configloc = configloc + "/.wisend/config.conf";

#endif // _WIN32
    std::ifstream File;
    File.open(configloc.c_str());
    if(File.is_open())
    {
        readConfig();
       File.close();
      return true;
    }
    else
    {
      File.close();
      #if _WIN32
      #else

     //std::string Cname = getenv("HOSTNAME");
     std::string Cname;
     std::ifstream Hostname;
     Hostname.open("/etc/hostname");
            if(Hostname.is_open())
            {
            char c;
            std::string buf="";
                while(Hostname.get(c))
                {
                    buf = buf + c;

                }
            Cname = buf;
            }
            else
            {
                std::cout<<"ERROR: No Hostname."<<std::endl;
                return false;
            }
     #endif // _WIN32

// this is a hack NEED TO BE REWRITTEN SO USERS CAN CHANGE IT!!!!!!!!!
     mkdir(savelll.c_str(),S_IRUSR | S_IWUSR | S_IXUSR);
     mkdir(saveLoc.c_str(),S_IRUSR | S_IWUSR | S_IXUSR);
     mkdir(config.c_str(),S_IRUSR | S_IWUSR | S_IXUSR);
     saveDirectory = saveLoc;
     std::ofstream file(configloc.c_str());
     std::cout<<"file: "<<configloc<<std::endl;
     file << "sertype=205;"<<std::endl;
     file << "Computername="<<Cname<<";"<<std::endl;
     file << "saveDirectory="<<saveDirectory<<";"<<std::endl;
    }

     Broad_LOOP();
   // return true;
}

bool server::readConfig()
{
 std::string configloc = "";
#if _WIN32
configloc = getenv("APPDATA");
configloc = configloc + "\\.wisend\\config.conf";
#else
configloc = getenv("HOME");
configloc = configloc + "/.wisend/config.conf";

#endif // _WIN32
std::ifstream file(configloc.c_str());

    if(file.is_open())
    {
      char c;
      std::string buff="";

      while(file.get(c))
      {
        buff = buff + c;
      }

      Loadsetts(buff);
      Broad_LOOP();
    }
    else
    {

    }

}

bool server::Loadsetts(std::string token)
{
//TODO: NEEDS SO I CAN MAKE THE CONF READABLE;
 size_t len;
 uint8_t varflag; //only 255 options supported :^)
 len = token.length();
 std::string temp = "";
 bool foundequal = false;
    for(size_t i=0; i<len;i++)
    {
     //temp = token.substr(i,1)
     std::cout<<"tempis: "<<temp<<std::endl;
      if(token.substr(i,1) == "=")
      {
        foundequal = true;
        std::cout<<"Token: "<<temp<<std::endl;
        if(temp == "sertype")
        {
          varflag = 1;
          //Flush temp
          temp = "";
        }
        else if(temp == "Computername")
        {
         varflag = 2;
         temp = "";
        }
        else if(temp == "saveDirectory")
        {
         varflag = 3;
         temp="";
        }
        else
        {
        std::cout<<"WARNING: Unused setting varible '"<<temp<<"'"<<std::endl;
         varflag = 253;
         temp ="";
        }
      }
      else if(token.substr(i,1) == ";")
      {
       if(foundequal == false)
       {
        std::cout<<"ERROR: MALFORMED CONFIGFILE"<<std::endl;
        return false;
       }
        if(varflag == 1)
        {
         readableSertype = atoi(temp.c_str());
         serType = readableSertype;
          std::cout<<"temp: "<<temp<<"serType: "<<serType<<std::endl;
         temp = "";
         foundequal == false;
        }
        else if(varflag == 2)
        {
         Computername = temp;
         temp = "";
         foundequal == false;

        }
        else if(varflag == 3)
        {
         saveDirectory = temp;
         temp ="";
         foundequal == false;
        }
        else if(varflag == 253)
        {
         std::cout<<"Warning ignored since INVALID VARIBLE";
         std::cout<<"Temp is: "<<temp<<std::endl;
         temp = "";
         foundequal == false;
        }

      }
      else if(token.substr(i,2) == "\r"||token.substr(i,2) == "\n"||token.substr(i,1) == "")
      {
      //do nuffin
      std::cout<<"in \r \n "<<std::endl;
      }
      else
      {
       temp = temp + token.substr(i,1);
       std::cout<<"old temp: "<<temp<<std::endl;
      }
    }

    std::cout<<"serType: "<<readableSertype<<" Computername: "<<Computername<<" saveDirectory: "<<saveDirectory<<std::endl;
    return true;

}

void server::Broadcast()
{
   //get packet ready
   pack << serType << State << Computername;
   unsigned short port = 1911;

  if(udp.send(pack,sf::IpAddress::Broadcast,port)!= sf::Socket::Done)
  {
  std::cout<<"ERROR SENDING PACKET"<<std::endl;

  }
  else
  {
  std::cout<<"SUCCESS: Packet sent"<<std::endl;
  }
  sleep(5);
}

void server::Broad_LOOP()
{

    while(1)
    {
        Broadcast();
    }
}

void server::Listen()
{

}

server::~server()
{
    //dtor
}
